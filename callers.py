import os
import sys
import hashlib
import secrets
from datetime import datetime
import logging

import aiosql
import sqlite3
import redis
from twilio.rest import Client


TWILIO_ACCOUNT_SID = os.environ['TWILIO_ACCOUNT_SID']
TWILIO_AUTH_TOKEN = os.environ['TWILIO_AUTH_TOKEN']
SCREAM_MEDIA_DIR = os.environ.get('SCREAM_MEDIA_DIR', '.')
SCREAM_HASH_KEY = bytes.fromhex(os.environ.get('SCREAM_CALLER_HASH_KEY_HEX'))
SERVER_NAME = os.environ.get('SERVER_NAME')


twilio_client = Client(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN)

redis_db = redis.Redis()

queries = aiosql.from_path('callers.sql', 'sqlite3')

logging.basicConfig(filename='calls.log', level=logging.INFO)

def connect():
    db = sqlite3.connect('screamdb.sql')
    db.row_factory = sqlite3.Row
    return db


def generate_caller_hash(caller):
    caller_hash = hashlib.blake2b(
        caller.encode(),
        key=SCREAM_HASH_KEY,
        digest_size=20,
    )
    return caller_hash.hexdigest()

def generate_caller_hex_id():
    hex_id = secrets.token_hex(8)
    return hex_id

def generate_token(caller_hash):
    with connect() as db:
        caller = queries.get_caller_by_hash(db, caller_hash)
    new_token = secrets.token_urlsafe(64)
    redis_db.set(caller['hex_id'], new_token, ex=86400)
    return caller['id'], caller['hex_id'], new_token

def validate_token(hex_id, token):
    is_valid = (redis_db.get(hex_id) == token.encode())
    return is_valid

def store_call_recording(call_sid, recording_sid, call_id, recording_id):
    with connect() as db:
        return queries.insert_call_recording(
            db, 
            call_sid=call_sid,
            recording_sid=recording_sid,
            call_id=call_id,
            recording_id=recording_id,
        )

def stream_call_list(start_dt=datetime(2020, 10, 1), end_dt=None):
    calls = twilio_client.calls.stream(
                     start_time_after=start_dt,
                     start_time_before=end_dt,
                     page_size=200,
                 )
    return calls

def store_call_and_caller(call):
    '''Returns call id.'''
    # See https://support.twilio.com/hc/en-us/articles/
    # 223179988-Why-am-I-getting-calls-from-these-strange-numbers-
    is_anonymous = 0
    if call.from_ in (
        '+7378742833',   # RESTRICTED  
        '+2562533',      # BLOCKED     
        '+8566696',      # UNKNOWN     
        '+266696687',    # ANONYMOUS   
        '+86282452253',  # UNAVAILABLE 
        '+464',          # No Caller ID
    ):
        is_anonymous = 1
    caller_hash = generate_caller_hash(call.from_)
    logging.info(f'Storing call {call.sid} and caller {caller_hash}')
    with connect() as db:
        caller_id = queries.get_caller_id_by_hash(db,
            caller_hash=caller_hash,
        )
        if caller_id is None:
            caller_id = queries.insert_caller(db, 
                caller_hash=caller_hash,
                hex_id=generate_caller_hex_id(),
                is_anonymous=is_anonymous,
            )
            print(f'New caller ID: {caller_id}')
        call_id = queries.get_call_id_by_sid(db,
            sid=call.sid,
        )
        if call_id is None:
            call_id = queries.insert_call(db,
                sid=call.sid,
                caller_hash=caller_hash,
                caller_id=caller_id,
                start_time=call.start_time,
                end_time=call.end_time,
                duration=call.duration,
                price=call.price,
                status=call.status,
            )
            print(f'New call ID: {call_id}')
    return call_id


def get_caller_by_hex_id(hex_id):
    with connect() as db:
        caller = queries.get_caller_by_hex_id(
            db,
            hex_id
        )
    return caller

def get_caller_by_phone_number(phone):
    caller_hash = generate_caller_hash(phone)
    with connect() as db:
        caller = queries.get_caller_by_hash(
            db,
            caller_hash,
        )
    return caller
    
def get_recordings_from_caller(caller):
    with connect() as db:
        recordings = {}
        recordings['ok'] = queries.get_caller_recordings_ok(
            db,
            caller_id=caller['id'],
        )
        recordings['queued'] = queries.get_caller_recordings_queued(
            db,
            caller_id=caller['id'],
        )
        recordings['no'] = queries.get_caller_recordings_no(
            db,
            caller_id=caller['id'],
        )
    return recordings

def generate_url_for_caller(caller):
    token = generate_token(caller['hash'])
    return f'https://my.{SERVER_NAME}/{caller["hex_id"]}?t={token[2]}'


