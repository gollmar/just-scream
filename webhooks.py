import os
from functools import wraps

from flask import Flask, request, abort, session
from twilio.twiml.voice_response import *
from twilio.twiml.messaging_response import MessagingResponse
from twilio.request_validator import RequestValidator


from callers import get_caller_by_phone_number, generate_url_for_caller


app = Flask(__name__)
app.secret_key = bytes.fromhex(os.environ.get('SECRET_KEY'))

TWILIO_AUTH_TOKEN = os.environ.get('TWILIO_AUTH_TOKEN')


def validate_twilio_request(f):
    '''Validates that incoming requests genuinely originated from Twilio'''
    @wraps(f)
    def decorated_function(*args, **kwargs):
        validator = RequestValidator(TWILIO_AUTH_TOKEN)
        # Validate the request using its URL, POST data,
        # and X-TWILIO-SIGNATURE header
        request_valid = validator.validate(
            request.url,
            request.form,
            request.headers.get('X-TWILIO-SIGNATURE', ''))

        if request_valid or request.method == 'GET':
            return f(*args, **kwargs)
        else:
            return abort(403)
    return decorated_function


@app.route('/')
def index():
    return 'OK!'


@app.route('/call/menu', methods=['POST'])
@validate_twilio_request
def main_menu():
    resp = VoiceResponse()
    gather = Gather(action='/call/menu/choice', method='POST', numDigits=1)
    gather.say('''
        To scream, press 1.
        To record a message of hope to share, press 2.
        To listen to today's message of hope, press 3.
    ''')
    gather.pause(length=5)
    gather.say('''
        To scream, press 1.
        To record a message of hope to share, press 2.
        To listen to today's message of hope, press 3.
    ''')
    resp.append(gather)
    resp.say('Goodbye!')
    resp.hangup()
    return str(resp)

@app.route('/call/menu/choice', methods=['POST'])
@validate_twilio_request
def menu_choice():
    digits = request.values.get('Digits')
    resp = VoiceResponse()

    # Scream into the silence:
    if digits == '1':
        resp.say('''
            We aren't recording, but you can still scream!
            Here's 7 seconds of silence.
        ''')
        resp.pause(length=7)
        resp.say('Goodbye!')
        resp.hangup()
        return str(resp)


    # Record a new message of hope:
    if digits == '2':
        resp.say('''
            Thank you for adding your voice to this project.
            Record your message after the beep, then hang up or
            press pound to return to the main menu.
        ''')
        resp.record(action='/call/thank-you', method='POST', maxLength=300)
        resp.hangup()
        return str(resp)

    # Listen to a message of hope:
    if digits == '3':
        resp.say('This is today\'s message.')
        resp.pause()
        resp.play('https://justscream.baby/screams/RE0896a148edd9c49aa6204f627a6ed270.mp3')
        resp.pause()
        gather = Gather(action='/call/menu', method='POST', finishOnKey='', numDigits=1)
        gather.say('''
            Thank you for listening. To return to the main menu,
            press pound.
        ''')
        gather.pause()
        resp.append(gather)
        resp.hangup()
        return str(resp)

    # If all else fails...
    gather = Gather(action='/call/menu/choice', method='POST', numDigits=1)
    gather.say('''
        That is not an option.
        Please enter option 1, 2, or 3.
    ''')
    gather.pause(length=2)
    resp.append(gather)
    resp.hangup()
    return str(resp)


@app.route('/call/thank-you', methods=['POST'])
@validate_twilio_request
def thank_you():
    resp = VoiceResponse()
    resp.say('''
        Thank you! Your recording will soon be available on 
        just scream dot baby. You may hang up to leave this call
        or stay on the line to return to the main menu.
    ''')
    resp.pause(length=5)
    resp.redirect('/call/menu', method='POST')
    return str(resp)

@app.route('/msg', methods=['POST'])
@validate_twilio_request
def msg_reply():
    body = request.values.get('Body')
    resp = MessagingResponse()
    if body.strip().lower() == 'list':
        caller = get_caller_by_phone_number(request.values.get('From'))
        if caller is None:
            resp.message('Thanks for checking out JUST SCREAM. We have no record of any calls \
from this phone number')
            return str(resp)
        if caller['is_anonymous']:
            resp.message('Thanks for checking out JUST SCREAM. You messaged from an anonymous \
or blocked number, so we won\'t be able to list your recordings')
            return str(resp)
        # Caller exists! That doesn't mean they have any recordings, but we can still
        # send them a link:
        resp.message('Here is a link to find your screams. It\'s valid for the next 24 hours')
        resp.message(generate_url_for_caller(caller))
        return str(resp)

    # User is not requesting a list of calls:
    if session.get('reply'):
        # We've already replied to this user. Send an empty response
        return str(resp)
    session['reply'] = True
    resp.message('Hi! This is JUST SCREAM. \
Learn more at https://justscream.baby or email info@justscream.baby')
    return str(resp)
