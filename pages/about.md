---
title_short: About
title_long: About Just Scream
slug: about
description: Just Scream is a participatory sound art project created by Chris
    Gollmar.  
---

I am an elementary school teacher by profession but have also been an artist and
coder since I was a kid. Over the past several years, I've created a few
participatory art projects that invite people to call a phone number and leave a
voicemail for others to hear. In each project, the constraint was different --
in Listening Point, I asked participants to narrate what they heard in their
immediate environment; in NOISE, I instructed participants to speak for two
minutes continuously; in Call-In Special, participants could record anything
they wanted.

In September 2020, I decided to create a new piece in this format. It didn't
take long to settle on a theme and a name: Just Scream! I designed and coded
this site, launching it just before Election Day in the U.S. 







### Media Coverage

Just Scream has been featured all around the world.

* [I found solace in scream hotlines (and you can too)](https://www.salon.com/2021/01/08/i-found-solace-in-scream-hotlines-and-you-can-too/) (Salon.com)
* [Scream into the void until Inauguration Day with Just Scream](https://mashable.com/article/just-scream-baby-phone-hotline-chris-gollmar/) (Mashable)
* [Call this phone number to
  scream](https://boingboing.net/2020/11/22/call-this-phone-number-to-scream.html)
  (Boing Boing)
* [Good Day Sacramento](https://youtu.be/_3VPsLoZkwo) (Sacramento, CA)
* [Lyst til at skrige ad 2020? Så ring til dette
  nummer](https://jyllands-posten.dk/kultur/ECE12616449/lyst-til-at-skrige-ad-2020-saa-ring-til-dette-nummer/)
  (Jyllands-Posten, Copenhagen, DK)
* [BFMTV](https://twitter.com/BFMTV/status/1334211199614013446) (France)


### Other Scream Projects

Since launching Just Scream, I have learned of many more scream-related projects
around the world. Here's what I've found so far. Please [contact
me](mailto:info@justscream.baby) if you know of others!

* [Let It Out](https://lookslikeyouneediceland.com/) - Record a scream to be
  played in Iceland.
* [Scream Day](https://www.screamday.com/) - A project to create a new holiday.
* [Negotiation Between Madness & Sanity](https://filmpro.net/portfolio/zeynep) -
  An ongoing project by filmmaker Zeynep Dagli featuring people screaming on
  camera.
* [ScreamBody](https://web.media.mit.edu/~monster/screambody/) - A wearable, portable space for screaming by Kelly Dobson

