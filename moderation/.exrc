" Use actual tabs
set noexpandtab
set shiftwidth=8
set tabstop=8

" Play/Pause with space and ctrl-space
"nnoremap <Space> ma0w:let jid = jobstart('play https://justscream.baby/screams/'.expand('<cfile>')) <CR>`a
nnoremap <Space> ma0w:let jid = jobstart('play ../screams/'.expand('<cfile>')) <CR>`a
nnoremap <C-Space> :call jobstop(jid) <CR>

" Navigate between tab-delimited fields
nnoremap <Tab> f<Tab>l
nnoremap <S-Tab> F<Tab>T<Tab>

" Quick adding of playlists/tabs
inoremap #n #nsfw <Esc>
inoremap #h #hope <Esc>
inoremap #l #lol <Esc>
inoremap #r #rawr <Esc>
inoremap #b #baby <Esc>

nmap #n A #n
nmap #h A #h
nmap #l A #l
nmap #r A #r
nmap #b A #b

" Add ds command to "delete scream" (really move it to bottom of the file)
nnoremap ds ddmaGp'a

" Add s commands for changing the moderation status
nmap sk 0<Tab><Tab><Tab>ciwok<Esc>
nmap sn 0<Tab><Tab><Tab>ciwno<Esc>
nmap sd 0<Tab><Tab><Tab>ciwdelete<Esc>
nmap sq 0<Tab><Tab><Tab>ciwqueued<Esc>
