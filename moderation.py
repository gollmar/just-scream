import csv
from datetime import datetime
import re

import frontmatter
import pendulum

import recordings


def get_moderation_queue(queue, limit=-1):
    with recordings.connect() as db:
        return recordings.queries.get_moderation_queue_with_recordings(
            db, 
            queue=queue,
            limit=limit,
            )

def update_moderation_status(recording_id, status, notes):
    with recordings.connect() as db:
        _ = recordings.queries.update_moderation_status(
            db, 
            recording_id=recording_id, 
            status=status,
            notes=notes,
        )
        db.commit()
        mod_status = recordings.queries.get_moderation_status(
            db,
            recording_id=recording_id,
        )
    return mod_status

def moderate_backlog(num_recordings=100, destination=None):
    '''Select a random sample of the backlog and create a moderation file
    with destination=dawn-chorus_{ts}.'''
    if destination is None:
        destination_date = pendulum.now().start_of('week').add(weeks=1)
        destination = f'dawn-chorus_{destination_date.isoformat()}'
    with recordings.connect() as db:
        recordings.queries.change_moderation_queue_random_sample(
            db,
            limit=num_recordings,
            queue=destination,
        )
    rec_list = get_moderation_queue(destination)
    filename = make_moderation_file(rec_list, destination=destination)
    return filename

def moderate_new_recordings(num_recordings=100):
    '''Stream new recordings and create a moderation file with 
    destination=hope.'''
    rec_list = get_moderation_queue('hope', limit=num_recordings)
    filename = make_moderation_file(rec_list, destination='hope')
    return filename

def make_moderation_file(rec_list, destination):
    '''Creates a text file to be used for screening recordings before they are
    published.'''
    ts = datetime.now().isoformat()

    content = '\n'.join(
        (f'{rec["id"]}\t{rec["filename_mp3"]}\t{rec["duration"]}s\t{rec["status"]}\t' for rec in rec_list)
    )
    content += '\n---'

    p = frontmatter.Post(content)
    p['date'] = ts
    p['destination'] = destination

    filename = f'moderation_{destination}_{ts}.txt'
    frontmatter.dump(p, filename)
    return filename


def parse_moderation_file(filename):
    m = frontmatter.load(filename)
    m_primary, m_secondary = m.content.split('---\n')
    m_primary_lines = csv.DictReader(
        m_primary.split('\n'),
        delimiter='\t',
        fieldnames=[
            'recording_id',
            'filename_mp3',
            'duration',
            'mod_status',
            'notes',
        ],
    )
    m_secondary_lines = csv.DictReader(
        m_secondary.split('\n'),
        delimiter='\t',
        fieldnames=[
            'recording_id',
            'filename_mp3',
            'duration',
            'mod_status',
            'notes',
        ],
    )
    return {
        'meta': m.metadata,
        'primary': m_primary_lines,
        'secondary': m_secondary_lines,
    }

def process_moderation_file(filename):

    mod_data = parse_moderation_file(filename)

    recordings_updated = set() 
    slugs_updated = set() 
    dates_updated = set()
    months_updated = set()

    def get_slugs(line, add_destination=None):
        if line['notes'] is None:
            return []
        slugs = re.findall('#(\w+)', line['notes'])
        if add_destination is not None:
            slugs.append(add_destination)
        return slugs

    # Get primary playlist if it exists, else prep SQL statement to create
    primary_playlist = recordings.get_playlist_by_slug(mod_data['meta']['destination'])
    if primary_playlist is None:
        # TODO: Create the playlist!
        print(f'Playlist #{mod_data["meta"]["destination"]} must be created first.')
        return 1
    else:
        print(f'Playlist #{mod_data["meta"]["destination"]} selected.')

    # Prep SQL statements for primary lines 
    for line in mod_data['primary']:
        slugs = get_slugs(line, add_destination=mod_data['meta']['destination'])
        update_moderation_status(
            line['recording_id'],
            line['mod_status'],
            line['notes'],
        )
        recordings_updated.add(line['recording_id'])
        recordings.update_recording_playlists(
            line['recording_id'],
            slugs,
        )
        slugs_updated.update(set(slugs))

    # Prep SQL statements for secondary lines
    for line in mod_data['secondary']:
        slugs = get_slugs(line)
        update_moderation_status(
            line['recording_id'],
            line['mod_status'],
            line['notes'],
        )
        recordings_updated.add(line['recording_id'])
        recordings.update_recording_playlists(
            line['recording_id'],
            slugs,
        )
        slugs_updated.update(set(slugs))

    for recording_id in recordings_updated:
        rec = recordings.get_recording_by_id(recording_id)
        dates_updated.add(rec['date_created'][:10])
        months_updated.add(rec['date_created'][5:7])

    return {
        'recordings_updated': recordings_updated,
        'slugs_updated': slugs_updated,
        'dates_updated': dates_updated,
        'months_updated': months_updated,
    }

