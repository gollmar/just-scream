create table update_metadata_new (
    id integer primary key autoincrement,
    update_datetime datetime not null default current_timestamp
);

insert into update_metadata_new
select * from update_metadata;

drop table update_metadata;

alter table update_metadata_new
rename to update_metadata;


create table moderation_new (
    id integer primary key autoincrement,
    recording_id integer unique,
    queue text default 'default',
    status default 'queued',
    notes text,
    update_metadata_id, 
    check (status in ('queued', 'ok', 'no', 'delete')),
    foreign key (recording_id) references recordings(id),
    foreign key (update_metadata_id) references update_metadata(id)
);

insert into moderation_new
select * from moderation;

drop table moderation;

alter table moderation_new
rename to moderation;

-- Remove my sweet message:
delete from recordings where id=999999;
