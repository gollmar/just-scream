-- name: create_table_callers!
create table callers (
    id integer primary key autoincrement,
    hash text unique,
    hex_id text unique,
    is_anonymous integer,
    check (is_anonymous in (0, 1))
);

-- name: create_table_call_recordings!
create table call_recordings (
    id integer primary key autoincrement,
    call_sid integer,
    recording_sid integer,
    call_id integer,
    recording_id integer,
    foreign key (call_sid) references calls(sid),
    foreign key (recording_sid) references recordings(sid),
    foreign key (call_id) references calls(id),
    foreign key (recording_id) references recordings(id)
);

-- name: create_table_calls!
create table calls (
    id integer primary key autoincrement,
    sid text unique,
    caller_hash text not null,
    caller_id integer not null,
    start_time datetime,
    end_time datetime,
    duration integer,
    price real,
    status text,
    foreign key (caller_hash) references callers(hash),
    foreign key (caller_id) references callers(id)
);


-- name: insert_caller<!
insert or replace into callers (
    hash,
    hex_id,
    is_anonymous
) values (
    :caller_hash,
    :hex_id,
    :is_anonymous
);


-- name: insert_call_recording!
insert or ignore into call_recordings (
    call_sid,
    recording_sid,
    call_id,
    recording_id
) values (
    :call_sid,
    :recording_sid,
    :call_id,
    :recording_id
);

-- name: get_call_id_by_sid$
-- Returns just the call_id
select id from calls where sid = :sid;

-- name: insert_call<!
insert into calls (
    sid,
    caller_hash,
    caller_id,
    start_time,
    end_time,
    duration,
    price,
    status
) values (
    :sid,
    :caller_hash,
    :caller_id,
    :start_time,
    :end_time,
    :duration,
    :price,
    :status
);

-- name: get_caller_id_by_hash$
select id from callers where hash = :caller_hash;

-- name: get_caller_by_hash^
select * from callers where hash = :caller_hash;

-- name: get_caller_by_hex_id^
select * from callers where hex_id = :hex_id;

-- name: get_caller_recordings_ok
-- Select calls that are not in spam and have a moderation status of null
-- or ok.
select
    r.id,
    r.sid,
    r.uri,
    r.date_created,
    r.duration,
    r.filename_mp3,
    r.filename_wav,
    t.text,
    case
        when s.id is null then 0
        else 1
    end is_spam,
    m.status
from recordings as r
    left outer join transcriptions as t
        on t.recording_id = r.id
    left outer join update_metadata as u
        on r.update_metadata_id = u.id
    left outer join spam as s
        on s.recording_id = r.id
    left outer join moderation as m
        on m.recording_id = r.id
    inner join call_recordings as cr
        on cr.recording_id = r.id
    inner join calls
        on calls.id = cr.call_id
    inner join callers
        on callers.hash = calls.caller_hash
    where callers.id = :caller_id
        and m.status = 'ok'
        and s.id is null
    order by r.id desc;

-- name: get_caller_recordings_queued
-- Select calls that have a moderation status of 'queued'
select
    r.id,
    r.sid,
    r.uri,
    r.date_created,
    r.duration,
    r.filename_mp3,
    r.filename_wav,
    t.text,
    case
        when s.id is null then 0
        else 1
    end is_spam,
    m.status
from recordings as r
    left outer join transcriptions as t
        on t.recording_id = r.id
    left outer join update_metadata as u
        on r.update_metadata_id = u.id
    left outer join spam as s
        on s.recording_id = r.id
    left outer join moderation as m
        on m.recording_id = r.id
    inner join call_recordings as cr
        on cr.recording_id = r.id
    inner join calls
        on calls.id = cr.call_id
    inner join callers
        on callers.hash = calls.caller_hash
    where callers.id = :caller_id
        and m.status = 'queued'
    order by r.id desc;

-- name: get_caller_recordings_no
-- Select calls that have a moderation status of 'queued'
select
    r.id,
    r.sid,
    r.uri,
    r.date_created,
    r.duration,
    r.filename_mp3,
    r.filename_wav,
    t.text,
    case
        when s.id is null then 0
        else 1
    end is_spam,
    m.status
from recordings as r
    left outer join transcriptions as t
        on t.recording_id = r.id
    left outer join update_metadata as u
        on r.update_metadata_id = u.id
    left outer join spam as s
        on s.recording_id = r.id
    left outer join moderation as m
        on m.recording_id = r.id
    inner join call_recordings as cr
        on cr.recording_id = r.id
    inner join calls
        on calls.id = cr.call_id
    inner join callers
        on callers.hash = calls.caller_hash
    where callers.id = :caller_id
        and m.status in ('no', 'delete')
    order by r.id desc;

-- name: get_latest_call_dt$
-- Get the timestamp of the latest call
select max(start_time) from calls;
