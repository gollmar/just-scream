import os
from datetime import datetime, timedelta
from pprint import pprint
import sqlite3

from flask import Flask, render_template, url_for, send_file, abort, redirect, request
import aiosql
import markdown
import frontmatter

import recordings
import callers


app = Flask(__name__, subdomain_matching=True)

md = markdown.Markdown(extensions=['smarty', 'meta'])

SCREAM_MEDIA_DIR = os.environ.get('SCREAM_MEDIA_DIR')
SERVER_NAME = os.environ.get('SERVER_NAME')
SERVER_PORT = os.environ.get('SERVER_PORT')

app.config['SERVER_NAME'] = f'{SERVER_NAME}:{SERVER_PORT}'

@app.template_filter()
def format_datetime(iso_dt, with_time=False):
    dt = datetime.fromisoformat(iso_dt)
    if with_time:
        return dt.strftime('%b. %-d, %Y, %-H:%M %Z')
    return dt.strftime('%b. %-d, %Y')


@app.route('/hope')
def index():
    return render_template('index.html')


@app.route('/')
def hope():
    return render_template('hope.html')

@app.route('/listen/')
def listen_index():
    playlists = recordings.get_playlists()
    return render_template('listen_index.html', 
        playlists=playlists, 
        total_track_count=recordings.get_track_count(),
    )


@app.route('/listen/new/')
def listen_latest():
    rec_list = recordings.get_recordings(
        start_dt=datetime(2021, 1, 14),
        limit=-1,
        new=True,
    )
    return render_template('listen.html',
        rec_list=rec_list,
        page=None,
        latest_update_dt=datetime.now(),
        spam=False,
        new=True,
    )


@app.route('/listen/all/')
def listen_all_top():
    return listen_all(1)


@app.route('/listen/all/<int:page>/')
def listen_all(page):
    spam = False
    if 'spam' in request.args:
        spam = True
    rec_list = recordings.get_recordings(page=page, spam=spam)
    if len(rec_list) == 0:
        abort(404, 'We could use a few more screams here. Call 561-576-8431 to record a new one.')
    return render_template('listen.html', 
        rec_list=rec_list, 
        page=page,
        latest_update_dt=datetime.now(),
        spam=spam,
    )


@app.route('/listen/<slug>/')
def listen_playlist(slug):
    rec_list = recordings.get_recordings(slug=slug)
    playlist = recordings.get_playlist_by_slug(slug)
    if len(rec_list) == 0:
        abort(404, 'We could use a few more screams here. Call 561-576-8431 to record a new one.')
    return render_template('listen.html', 
        rec_list=rec_list, 
        page=None,
        latest_update_dt=datetime.now(),
        spam=False,
        playlist=playlist,
    )


@app.route('/<int:recording_id>/')
def listen_scream(recording_id):
    rec = recordings.get_recording_by_id(recording_id)
    if rec is None:
        abort(404, 'Sorry, we could not find the recording you were looking for.')
    return render_template('scream.html',
        rec=rec,
        date_created_dt=datetime.fromisoformat(rec['date_created']),
    )


@app.route('/screams/<filename>')
def send_scream_file(filename):
    return send_file(os.path.join(SCREAM_MEDIA_DIR, 'screams', filename))


@app.route('/about/')
def page_about():
    with open('pages/about.md', 'r') as f:
        meta, content_md = frontmatter.parse(f.read())
        content_html = md.convert(content_md)
    return render_template('page.html', meta=meta, content=content_html)


@app.route('/', subdomain='my')
def my_index():
    return render_template('my_index.html')



@app.route('/<hex_id>', subdomain='my')
def my_screams(hex_id):
    token = request.values.get('t')
    if token is None:
        return render_template('my_error.html')
    if not callers.validate_token(hex_id, token):
        return render_template('my_error.html')
    caller = callers.get_caller_by_hex_id(hex_id)
    recordings = callers.get_recordings_from_caller(caller)
    return render_template('my_screams.html', recordings=recordings)
