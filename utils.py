import sys
from datetime import datetime
from collections import Counter
import click

import gatekeeper
import callers 
import recordings 
import moderation


@click.group()
def cli():
    '''Simple utilities for managing and updating Just Scream!'''
    pass

@cli.command()
def init_gatekeeper_defaults():
    '''Initializes default TwiML reponses and call frequency to local Redis DB.'''
    # Init values at 1 call every 45 seconds
    gatekeeper.redis_db.set('config:lookback_sec', 45)
    gatekeeper.redis_db.set('config:max_calls', 1)
    gatekeeper.redis_db.set('twiml:reject', 
        '''<?xml version="1.0" encoding="UTF-8"?>
        <Response>
          <Reject reason="busy" />
        </Response>'''
    )
    gatekeeper.redis_db.set('twiml:deny', 
        '''<?xml version="1.0" encoding="UTF-8"?>
        <Response>
          <Reject reason="rejected" />
        </Response>'''
    )
    gatekeeper.redis_db.set('twiml:ok', 
        '''<?xml version="1.0" encoding="UTF-8"?>
        <Response>
          <Redirect>https://antiphony.scream.baby/calls/</Redirect>
        </Response>'''
    )
    gatekeeper.redis_db.set('twiml:allowlist', 
        '''<?xml version="1.0" encoding="UTF-8"?>
        <Response>
          <Redirect>https://antiphony.scream.baby/calls/</Redirect>
        </Response>'''
    )
    return

@cli.command()
@click.option('--before', type=int, default=None, help='Time as POSIX timestamp')
def clear_gatekeeper_logs(before=None):
    '''Clears gatekeeper logs. If option before is not specified, clears all logs
    from before today at midnight.'''
    if before == None:
        midnight_today = datetime.today().replace(
            hour=0,
            minute=0,
            second=0,
            microsecond=0,
        )
        before = int(midnight_today.timestamp())
    print(f'Removing gatekeeper logs from before {datetime.fromtimestamp(before)}...')
    num_removed = gatekeeper.redis_db.zremrangebyscore(
        'calls:ok',
        0,
        before - 1)
    num_removed += gatekeeper.redis_db.zremrangebyscore(
        'calls:reject',
        0,
        before - 1)
    num_removed += gatekeeper.redis_db.zremrangebyscore(
        'calls:whitelist',
        0,
        before - 1)
    num_removed += gatekeeper.redis_db.zremrangebyscore(
        'calls:allowlist',
        0,
        before - 1)
    print(f'Successfully removed {num_removed} calls.')
    return


@cli.command()
def print_gatekeeper_logs():
    '''Prints all gatekeeper logs in the local Redis DB.'''

    def score_to_dt(score):
        return datetime.fromtimestamp(int(score))

    def print_call_list(call_list):
        for sid, dt in call_list:
            print(f'{dt}    {sid.decode()}')

    def print_count_by_date(call_list):
        print('## Counts by date:')
        c = Counter([dt.date() for sid, dt in call_list])
        for date, count in c.items():
            print(f'{date}: {count}')
        print()
        return

    def print_count_by_hour(call_list):
        print('## Counts by hour:')
        c = Counter([dt.hour for sid, dt in call_list])
        for hour, count in c.items():
                print(f'{hour:02}: {count}')
        print()
        return

    calls_ok = gatekeeper.redis_db.zrangebyscore(
        'calls:ok', 0, '+inf', 
        withscores=True, 
        score_cast_func=score_to_dt)
    calls_whitelist = gatekeeper.redis_db.zrangebyscore(
        'calls:whitelist', 0, '+inf', 
        withscores=True, 
        score_cast_func=score_to_dt)
    calls_allowlist = gatekeeper.redis_db.zrangebyscore(
        'calls:allowlist', 0, '+inf', 
        withscores=True, 
        score_cast_func=score_to_dt)
    calls_reject = gatekeeper.redis_db.zrangebyscore(
        'calls:reject', 0, '+inf', 
        withscores=True, 
        score_cast_func=score_to_dt)
    calls_deny = gatekeeper.redis_db.zrangebyscore(
        'calls:deny', 0, '+inf', 
        withscores=True, 
        score_cast_func=score_to_dt)
    print(f'Gatekeeper log retrieved {datetime.now()}\n')
    print(f'Call bins: {len(calls_ok)} ok / {len(calls_allowlist)} allowlist / {len(calls_reject)} reject/ {len(calls_deny)} deny\n\n')
    print(f'\n# OK Calls\n')
    print_count_by_date(calls_ok)
    print_count_by_hour(calls_ok)
    print_call_list(calls_ok)
    print(f'\n# Allowlisted Calls\n')
    print_count_by_date(calls_whitelist)
    print_count_by_hour(calls_whitelist)
    print_call_list(calls_whitelist)
    print(f'\n# Rejected Calls\n')
    print_count_by_date(calls_reject)
    print_count_by_hour(calls_reject)
    print_call_list(calls_reject)
    print(f'\n# Denied Calls\n')
    print_count_by_date(calls_deny)
    print_count_by_hour(calls_deny)
    print_call_list(calls_deny)

    return 

@cli.command()
@click.option('--start', type=str, default=None)
@click.option('--queue', type=str, default='default')
def fetch_and_store_calls(start=None, queue='default'):
    update_metadata_id = recordings.get_new_update_metadata_id()
    calls = callers.stream_call_list(start_dt=start)
    for call in calls:
        click.echo(f'Storing call {call.sid}')
        call_id = callers.store_call_and_caller(call)
        # Store recordings
        recs = call.recordings.stream()
        for rec in recs:
            click.echo(f'Storing recording {rec.sid}')
            recording_id = recordings.fetch_recording(
                update_metadata_id, 
                rec, 
                moderation_queue=queue,
            )
            callers.store_call_recording(
                call_sid=call.sid,
                recording_sid=rec.sid,
                call_id=call_id,
                recording_id=recording_id,
            )
    return

@cli.command()
@click.argument('queue', default='hope')
def moderate(queue):
    if queue == 'backlog':
        filename = moderation.moderate_backlog()
    if queue == 'hope':
        filename = moderation.moderate_new_recordings()
    click.echo(f'Moderation file created: {filename}')
    return

@cli.command()
@click.argument('filename')
@click.option('--out', 'output_filename', default=None)
def process_moderation_file(filename, output_filename):
    updates = moderation.process_moderation_file(filename)
    click.echo(f'Updated {len(updates["recordings_updated"])} recordings.')
    click.echo(f'Updated {len(updates["slugs_updated"])} playlists.')
    if output_filename is not None:
        with open(output_filename, 'w') as f:
            f.write(f'python build.py build-screams {" ".join(updates["recordings_updated"])}\n')
            f.write(f'python build.py build-playlists {" ".join(updates["slugs_updated"])}\n')
            f.write(f'python build.py build-dates {" ".join(updates["dates_updated"])}\n')
            f.write('python build.py build-months\n')
            f.write('python build.py build-home\n')
    return

    
if __name__ == '__main__':
    cli()
