-- name: create_table_recordings!
create table recordings (
    id integer primary key autoincrement,
    sid text not null,
    uri text not null,
    date_created datetime not null,
    update_metadata_id int not null,
    duration int not null,
    filename_mp3 text not null,
    filename_wav text not null,
    foreign key (update_metadata_id) references update_metadata(id)
);

-- name: create_table_recordings_temp!
create table recordings_temp (
    id integer primary key autoincrement,
    sid text not null,
    uri text not null,
    date_created datetime not null,
    duration int not null,
    filename_mp3 text not null,
    filename_wav text not null
);


-- name: create_table_transcriptions!
create table transcriptions (
    id integer primary key autoincrement,
    recording_id integer,
    text text,
    foreign key (recording_id) references recordings(id)
);

-- name: create_table_spam!
create table spam (
    id integer primary key autoincrement,
    recording_id integer not null,
    foreign key (recording_id) references recordings(id)
);

-- name: create_table_nsfw!
-- This is deprecated. NSFW screams are now part of a hidden playlist with 
-- slug='nsfw'.
create table nsfw (
    id integer primary key autoincrement,
    recording_id integer not null,
    foreign key (recording_id) references recordings(id)
);

-- name: create_view_nsfw!
create view nsfw (
    id,
    recording_id
) as select
    pr.id,
    pr.recording_id
from playlist_recordings as pr
inner join playlists as p
on pr.playlist_id = p.id
where p.slug = 'nsfw';

--name: create_table_deleted!
create table deleted (
    id integer primary key autoincrement,
    sid text not null
);

-- name: create_table_update_metadata<!
create table update_metadata (
    id integer primary key autoincrement,
    update_datetime datetime not null default current_timestamp
);


--name: create_table_playlists!
create table playlists (
    id integer primary key autoincrement,
    slug text not null unique,
    name text,
    description_short text,
    description_long text,
    is_published integer default 0
);


--name: create_table_playlist_recordings!
create table playlist_recordings (
    id integer primary key autoincrement,
    playlist_id integer,
    recording_id integer,
    foreign key (playlist_id) references playlists(id),
    foreign key (recording_id) references recordings(id)
);


--name: create_table_moderation!
create table moderation (
    id integer primary key autoincrement,
    recording_id integer unique,
    queue text default 'default',
    status default 'queued',
    notes text,
    update_metadata_id, 
    check (status in ('queued', 'ok', 'no', 'delete')),
    foreign key (recording_id) references recordings(id)
);


--name: create_table_deleted_recordings!
create table deleted_recordings (
    id integer primary key autoincrement,
    recording_id integer,
    recording_sid text
);


--name: create_table_build!
create table build (
    id integer primary key autoincrement,
    time_started datetime not null default current_timestamp,
    type text,
    mode text,
    notes text
);


-- name: save_recording_temp!
insert into recordings_temp (
    sid,
    uri,
    date_created,
    duration,
    filename_mp3,
    filename_wav
) values (
    :sid,
    :uri,
    :date_created,
    :duration,
    :filename_mp3,
    :filename_wav
);

-- name: truncate_recordings_temp
delete from recordings_temp;

-- name: save_recording_metadata<!
insert into recordings (
    sid,
    uri,
    date_created,
    update_metadata_id,
    duration,
    filename_mp3,
    filename_wav
) values (
    :sid,
    :uri,
    :date_created,
    :update_metadata_id,
    :duration,
    :filename_mp3,
    :filename_wav
);

-- name: save_transcription<!
insert into transcriptions values (
    :recording_id,
    :text
);

-- name: save_update_metadata<!
insert into update_metadata (id) 
values (null);

-- name: get_latest_update_datetime
select update_datetime
    from update_metadata
    inner join (select seq from sqlite_sequence where name="update_metadata") as latest
        on latest.seq = update_metadata.id

-- name: move_recordings_from_temp_table
insert into recordings (
    sid,
    uri,
    date_created,
    update_metadata_id,
    duration,
    filename_mp3,
    filename_wav
) select 
    r.sid,
    r.uri,
    r.date_created,
    :update_metadata_id,
    r.duration,
    r.filename_mp3,
    r.filename_wav
from recordings_temp as r
    order by r.date_created asc;


-- name: add_latest_recordings_to_moderation_queue
-- Adds the latest batch of recordings to the default moderation queue 
-- with status 'queued'.
insert into moderation (recording_id)
select r.id
from recordings as r
    left outer join update_metadata as u
        on r.update_metadata_id = u.id
        where u.id = (select max(id) from update_metadata);


-- name: change_moderation_queue_random_sample
-- Selects a random sample of recordings from the default moderation queue
-- with a status of 'queued' and changes their queue.
update moderation
set queue = :queue
where recording_id in (
    select recording_id
    from moderation
    where queue='default' and status='queued'
    order by random()
    limit :limit
);
        

-- name: get_moderation_queue_with_recordings
select 
    r.*,
    m.status,
    m.notes
from moderation as m
    inner join recordings as r
        on r.id = m.recording_id
where m.queue = :queue
and m.status = 'queued'
order by r.date_created
limit :limit;


-- name: get_moderation_status^
select * from moderation
where recording_id = :recording_id;


-- name: update_moderation_status<!
update moderation
set status = :status,
    notes = :notes
where recording_id = :recording_id;


--name: add_to_moderation_queue!
insert into moderation (recording_id, queue)
values (:recording_id, :queue);


-- name: get_recording_playlists
-- Gets a list of all playlists a recording is in
select p.* from playlists as p
inner join playlist_recordings as pr
    on p.id = pr.playlist_id
where pr.recording_id = :recording_id;


-- name: add_recording_to_playlist!
-- Adds a recording to a playlist
insert into playlist_recordings (recording_id, playlist_id)
select 
    :recording_id, 
    p.id 
from playlists as p
where p.slug = :playlist_slug;


-- name: remove_recording_from_playlist!
-- Removes a recording from a playlist
delete from playlist_recordings
where recording_id = :recording_id
and playlist_id = (
    select id 
    from playlists 
    where slug = :playlist_slug
);


-- name: get_all_recordings
-- Gets a list of all recordings 
-- Does not include spam; does include NSFW
select 
    r.id,
    r.sid,
    r.uri,
    r.date_created,
    u.update_datetime,
    r.duration,
    r.filename_mp3,
    r.filename_wav,
    t.text,
    case
        when u.id = (select max(id) from update_metadata) then 1
        else 0
    end is_latest,
    case
        when s.id is null then 0
        else 1
    end is_spam,
    case
        when n.id is null then 0
        else 1
    end is_nsfw
from recordings as r
    left outer join transcriptions as t
        on t.recording_id = r.id
    left outer join update_metadata as u
        on r.update_metadata_id = u.id
    left outer join spam as s
        on s.recording_id = r.id
    left outer join nsfw as n
        on n.recording_id = r.id
    where s.id is null
    order by r.id desc
    limit :limit offset :offset;

    
-- name: get_recordings_in_datetime_range
-- Gets a list of all recordings between :start_dt and :end_dt inclusive.
-- Only includes moderation status of 'ok'
select 
    r.id,
    r.sid,
    r.uri,
    r.date_created,
    r.duration,
    r.filename_mp3,
    r.filename_wav,
    t.text,
    case
        when n.id is null then 0
        else 1
    end is_nsfw
from recordings as r
    left outer join transcriptions as t
        on t.recording_id = r.id
    left outer join moderation as m
        on m.recording_id = r.id
    left outer join nsfw as n
        on n.recording_id = r.id
    where r.date_created between :start_dt and :end_dt
        and m.status = 'ok'
    order by r.id desc
    limit :limit offset :offset;

-- name: get_ok_recordings_by_date
select
    r.id,
    r.sid,
    r.uri,
    r.date_created,
    r.duration,
    r.filename_mp3,
    r.filename_wav,
    t.text,
    case
        when n.id is null then 0
        else 1
    end is_nsfw
from recordings as r
    left outer join transcriptions as t
        on t.recording_id = r.id
    left outer join moderation as m
        on m.recording_id = r.id
    left outer join nsfw as n
        on n.recording_id = r.id
    where date(r.date_created) = :date
        and m.status = 'ok'
    order by r.id desc
    limit :limit offset :offset;


-- name: get_all_ok_recordings
select
    r.id,
    r.sid,
    r.uri,
    r.date_created,
    r.duration,
    r.filename_mp3,
    r.filename_wav,
    t.text,
    case
        when n.id is null then 0
        else 1
    end is_nsfw
from recordings as r
    left outer join transcriptions as t
        on t.recording_id = r.id
    left outer join moderation as m
        on m.recording_id = r.id
    left outer join nsfw as n
        on n.recording_id = r.id
    where m.status = 'ok'
    order by r.id desc
    limit :limit offset :offset;

-- name: count_ok_recordings_by_date
-- Returns count of recordings with moderation status 'ok', grouped by date.
select 
    date(r.date_created) as date, 
    count(*) as count
from recordings as r
    left outer join moderation as m
        on m.recording_id = r.id
    where m.status = 'ok'
    group by date
    order by date;


-- name: sample_ok_recordings_by_date
-- Returns a random sample of recordings with moderation status 'ok' for the 
-- specified date. Sample will still be sorted in descending ID order.
select * from (
    select
        r.id,
        r.sid,
        r.uri,
        r.date_created,
        r.duration,
        r.filename_mp3,
        r.filename_wav,
        t.text,
        case
            when n.id is null then 0
            else 1
        end is_nsfw
    from recordings as r
        left outer join transcriptions as t
            on t.recording_id = r.id
        left outer join moderation as m
            on m.recording_id = r.id
        left outer join nsfw as n
            on n.recording_id = r.id
        where date(r.date_created) = :date
            and m.status = 'ok'
        order by random()
        limit :limit
) order by id desc;


-- name: get_spam_in_datetime_range
-- Gets a list of all spam recordings between :start_dt and :end_dt inclusive.
-- Only includes spam; does include NSFW recordings
select 
    r.id,
    r.sid,
    r.uri,
    r.date_created,
    u.update_datetime,
    r.duration,
    r.filename_mp3,
    r.filename_wav,
    t.text,
    case
        when u.id = (select max(id) from update_metadata) then 1
        else 0
    end is_latest,
    case
        when s.id is null then 0
        else 1
    end is_spam,
    case
        when n.id is null then 0
        else 1
    end is_nsfw
from recordings as r
    left outer join transcriptions as t
        on t.recording_id = r.id
    left outer join update_metadata as u
        on r.update_metadata_id = u.id
    left outer join spam as s
        on s.recording_id = r.id
    left outer join nsfw as n
        on n.recording_id = r.id
    where r.date_created between :start_dt and :end_dt
        and s.id is not null
    order by r.id desc
    limit :limit offset :offset;


--name: get_recording_by_id^
select *
from recordings
where id = :recording_id;

-- name: get_recording_id_by_sid$
select id from recordings where sid = :sid;


--name: get_playlist_by_slug^
select * 
from playlists
where slug = :slug;


--name: get_playlists
select p.*, count(pr.recording_id) as track_count
from playlists as p
left outer join playlist_recordings as pr
    on p.id = pr.playlist_id
where is_published = :is_published
group by p.id
order by id;


--name: get_playlist_recordings_by_slug
select 
    r.id,
    r.sid,
    r.uri,
    r.date_created,
    r.duration,
    r.filename_mp3,
    r.filename_wav,
    t.text,
    case
        when n.id is null then 0
        else 1
    end is_nsfw
from recordings as r
    left outer join transcriptions as t
        on t.recording_id = r.id
    left outer join moderation as m
        on m.recording_id = r.id
    left outer join nsfw as n
        on n.recording_id = r.id
    inner join (
        playlist_recordings as pr
        inner join playlists as p
            on p.id = pr.playlist_id
        ) 
        on pr.recording_id = r.id
    where p.slug = :slug
        and m.status = 'ok'
        and n.id is null
    order by r.id desc
    limit :limit offset :offset;

--name: get_track_count^
select count(*) as c
from recordings as r
inner join moderation as m
  on m.recording_id = r.id
where m.status = 'ok';

--name: log_build!
insert into build (type, mode, notes) 
values (:build_type, :mode, :notes);

--name: recordings_by_month
select
    cast(strftime('%Y', date_created) as integer) as year,
    cast(strftime('%m', date_created) as integer) as month,
    count(r.id) as recording_count
from recordings as r
group by year, month
order by year, month;

--name: calls_by_month
select
    cast(strftime('%Y', start_time) as integer) as year,
    cast(strftime('%m', start_time) as integer) as month,
    count(r.id) as call_count
from calls as c
group by year, month;

--name: unique_callers_by_month
select
    cast(strftime('%Y', calls.start_time) as integer) as year,
    cast(strftime('%m', calls.start_time) as integer) as month,
    count(distinct(callers.id)) as caller_count
from calls
inner join callers
on calls.caller_id = callers.id
group by year, month;

--name: recordings_by_unique_callers_by_month
select
    cast(strftime('%Y', date_created) as integer) as year,
    cast(strftime('%m', date_created) as integer) as month,
    count(distinct(c.caller_id)) as recording_count
from recordings as r
inner join call_recordings as cr
on cr.recording_id = r.id
inner join calls as c
on cr.call_id = c.id
group by year, month;

--name: avg_calls_by_caller
select avg(call_count) as avg_calls 
from (
    select
        caller_id,
        count(id) as call_count
    from calls
    group by caller_id
);

--name: avg_recordings_by_caller 
select avg(rec_count) as avg_recordings 
from (
    select
        c.caller_id,
        count(r.id) as rec_count
    from recordings as r
    inner join call_recordings as cr
    on cr.recording_id = r.id
    inner join calls as c
    on cr.call_id = c.id
    group by c.caller_id
);

--name: count_backlog_by_day
select 
    cast(strftime('%Y', r.date_created) as integer) as year,
    cast(strftime('%m', r.date_created) as integer) as month,
    cast(strftime('%d', r.date_created) as integer) as day,
    count(r.id) as recordings
from moderation as m
join recordings as r
on m.recording_id = r.id
where status = 'queued'
group by year, month, day;
