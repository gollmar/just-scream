import os
from functools import wraps

from flask import Flask, request, abort
import redis

from twilio.request_validator import RequestValidator


app = Flask(__name__)
redis_db = redis.Redis()

TWILIO_AUTH_TOKEN = os.environ.get('TWILIO_AUTH_TOKEN')





with open('gatekeeper.lua') as f:
    check_call = redis_db.register_script(f.read())


def validate_twilio_request(f):
    '''Validates that incoming requests genuinely originated from Twilio'''
    @wraps(f)
    def decorated_function(*args, **kwargs):
        validator = RequestValidator(TWILIO_AUTH_TOKEN)
        # Validate the request using its URL, POST data,
        # and X-TWILIO-SIGNATURE header
        request_valid = validator.validate(
            request.url,
            request.form,
            request.headers.get('X-TWILIO-SIGNATURE', ''))

        if request_valid or request.method == 'GET':
            return f(*args, **kwargs)
        else:
            return abort(403)
    return decorated_function


@app.route('/')
def index():
    return 'OK!'


@app.route('/call', methods=['GET', 'POST'])
@validate_twilio_request
def direct_call():
    '''Checks incoming calls against a pre-defined rate limit. Calls that
    are over the limit are rejected with a busy signal. Calls under the limit
    are sent to the IVR tree.'''

    if 'CallSid' not in request.values:
        abort(400, 'Missing SID')

    return check_call(args=[request.values['CallSid'], request.values.get('From')])

