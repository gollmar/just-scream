from glob import iglob
from datetime import datetime, date
import os
import math
from collections import defaultdict, Counter

import click
import jinja2
import frontmatter
import markdown
import pendulum

import recordings


md = markdown.Markdown(extensions=['smarty', 'meta'])

@click.group()
def cli():
    pass



'''Internal structure:

PATH                                            URI
{stage,build}/screams/<recording_id>.html       /<recording_id>/
{stage,build}/


Managed by another process:

<scream_media_dir>/screams/<filename>           /screams/<filename>

'''

# Filters for Jinja

def fmt_date(dt, fmt='short'):
    fmts = {
        'vshort': '%b. %-d',
        'short': '%b. %-d, %Y',
        'long': '%B %-d, %Y',
        'short+time': '%b. %-d, %Y at %-H:%M UTC',
        'time': '%-H:%M UTC',
    }
    if type(dt) == str:
        dt = datetime.fromisoformat(dt)
    return dt.strftime(fmts[fmt])

def date_url(dt):
    return f'/listen/{dt.isoformat()}'

jinja_env = jinja2.Environment(
    loader=jinja2.FileSystemLoader('templates'),
    autoescape=jinja2.select_autoescape(['html', 'xml']),
)

jinja_env.filters['fmt_date'] = fmt_date
jinja_env.filters['date_url'] = date_url


# Decorator to log Click commands to db

def log_build(f):
    def inner(*args, **kwargs):
        # Log in builds table
        click.echo(f'Logging build type: {f.__name__} with args {args} and kwargs {kwargs}')
        result = f(*args, **kwargs)
        with recordings.connect() as db:
            recordings.queries.log_build(db,
                build_type=f.__name__,
                mode=kwargs.get('mode'),
                notes=result,
            )
        click.echo(result)
    return inner


# Name the Click command, or else it will be called `inner` (from the log_build
# decorator)

@cli.command('build-home')
@log_build
def build_home():
    paths_built = []
    # Dict of templates
    routes = {
        # Path      Template filename
        '/':        'index.html',
        '/hope/':   'hope.html',
        '/listen/': 'listen_index.html',
    }
    for path, template_fn in routes.items():
        template = jinja_env.get_template(template_fn)
        output_fn = 'stage' + path
        if output_fn[-1] == '/':
            os.makedirs(output_fn, exist_ok=True)
            output_fn += 'index.html'
        click.echo(f'Writing {output_fn}')
        with open(output_fn, 'w') as f:
            f.write(template.render(
                path=path,
                total_track_count=recordings.get_track_count(),
                playlists=recordings.get_playlists(),
                )
            )
        paths_built.append(path)

    return f'Successfully built {", ".join(paths_built)}'


@cli.command('build-screams')
@click.option('-a', '--all', 'mode', flag_value='all', 
              help='generate all scream pages')
@click.option('-l', '--latest', 'mode', flag_value='latest', 
              help='generate pages for screams recorded since last build')
@click.option('-c', '--custom', 'mode', flag_value='custom', default=True,
              help='generate pages for specified recording_ids')
@click.argument('rec_id_list', nargs=-1) 
@log_build
def build_screams(mode, rec_id_list):
    '''Generates pages for single screams, given a list of recording_ids.'''
    # TODO: Should this also generate updated all screams pages? If so, how 
    # should those be organized?
    screams_built = 0
    template = jinja_env.get_template('scream.html')
    if mode == 'custom':
        rec_list = (recordings.get_recording_by_id(rec_id) for rec_id in rec_id_list)
    if mode == 'all':
        with recordings.connect() as db:
            rec_list = recordings.queries.get_all_ok_recordings(
                db,
                limit=-1,
                offset=0,
            )
    for rec in rec_list:
        with open(f'stage/{rec["id"]}.html', 'w') as f:
            f.write(template.render(
                rec=rec,
                date_created_dt=datetime.fromisoformat(rec['date_created']),
            ))
        screams_built += 1
    return f'Succesfully built {screams_built} screams'


@cli.command('build-pages')
@click.option('-a', '--all', 'mode', flag_value='all', 
              help='generate all pages')
@click.option('-c', '--custom', 'mode', flag_value='custom', default=True,
              help='generate pages for specified slugs')
@click.argument('slug_list', nargs=-1)
@log_build
def build_pages(mode, slug_list):
    pages_built = []
    template = jinja_env.get_template('page.html')
    for filename in iglob('pages/*.md'):
        with open(filename) as f:
            meta, content_md = frontmatter.parse(f.read())
            content_html = md.convert(content_md)
        if mode == 'all' or meta['slug'] in slug_list:
            click.echo(f'Writing page {meta["slug"]}')
            with open(f'stage/{meta["slug"]}.html', 'w') as f:
                f.write(template.render(meta=meta, content=content_html))
                pages_built.append(meta['slug'])
    return f'Successfuly built pages: {", ".join(pages_built)}'
            

@cli.command('build-playlists')
@click.option('-a', '--all', 'mode', flag_value='all',
              help='generate all playlists')
@click.option('-l', '--latest', 'mode', flag_value='latest',
              help='generate playlists with additions since last run')
@click.option('-c', '--custom', 'mode', flag_value='custom', default=True,
              help='generate playlists as specified by slug')
@click.argument('slug_list', nargs=-1)
@log_build
def build_playlists(mode, slug_list):
    # TODO: System for building latest
    playlists_built = []
    template = jinja_env.get_template('listen_playlist.html')
    playlists = recordings.get_playlists()
    for playlist in playlists:
        if mode == 'all' or playlist['slug'] in slug_list:
            rec_list = recordings.get_recordings(slug=playlist['slug'])
            click.echo(f'Writing playlist {playlist["slug"]}.')
            os.makedirs(f'stage/listen/{playlist["slug"]}', exist_ok=True)
            with open(f'stage/listen/{playlist["slug"]}/index.html', 'w') as f:
                f.write(template.render(
                    rec_list=rec_list, 
                    page=None,
                    latest_update_dt=datetime.now(),
                    spam=False,
                    playlist=playlist,
                    )
                )
                playlists_built.append(playlist['slug'])
    return f'Successfully built playlists: {", ".join(playlists_built)}'

@cli.command('build-dates')
@click.option('-a', '--all', 'mode', flag_value='all',
              help='generate all date indexes')
@click.option('-l', '--latest', 'mode', flag_value='latest',
              help='generate date indexes with additions since last run')
@click.option('-c', '--custom', 'mode', flag_value='custom', default=True,
              help='generate date indexes as specified')
@click.argument('date_list', nargs=-1, 
                callback=lambda ctx, d_list: [date.fromisoformat(d) for d in d_list])
@log_build
def build_dates(mode, date_list):
    dates_built = []
    template = jinja_env.get_template('listen_date.html')

    with recordings.connect() as db:
        dates_with_counts = recordings.queries.count_ok_recordings_by_date(db)

    for i, date_count in enumerate(dates_with_counts):
        rec_date = date.fromisoformat(date_count['date'])
        
        prev_date = None
        next_date = None
        if i > 1: 
            prev_date = date.fromisoformat(dates_with_counts[i - 1]['date'])
        if i < len(dates_with_counts) - 1:
            next_date = date.fromisoformat(dates_with_counts[i + 1]['date'])

        if mode == 'all' or rec_date in date_list:

            os.makedirs(f'stage/listen/{rec_date.isoformat()}', exist_ok=True)

            if date_count['count'] > 100:
                # Paginate! The first page is just a sample...

                num_pages = math.ceil(date_count['count'] / 50)

                with recordings.connect() as db:
                    sample_list = recordings.queries.sample_ok_recordings_by_date(
                        db,
                        date=rec_date,
                        limit=25,
                    )
                with open(f'stage/listen/{rec_date.isoformat()}/index.html', 'w') as f:
                    f.write(template.render(
                        date=rec_date,
                        sample=True,
                        rec_list=sample_list,
                        page=0,
                        num_pages=num_pages,
                        prev_date=prev_date,
                        next_date=next_date,
                        )
                    )
                
                # ...subsequent pages have 50 screams per page
                for page, offset in enumerate(range(0, date_count['count'], 50), start=1):
                    with recordings.connect() as db:
                        rec_list = recordings.queries.get_ok_recordings_by_date(
                            db,
                            date=rec_date,
                            limit=50,
                            offset=offset,
                        )

                    with open(f'stage/listen/{rec_date.isoformat()}/{page}.html', 'w') as f:
                        f.write(template.render(
                            date=rec_date,
                            rec_list=rec_list,
                            page=page,
                            num_pages=num_pages,
                            prev_date=prev_date,
                            next_date=next_date,
                            )
                        )
                dates_built.append(rec_date)

            else:
                # Put it all on a single page
                with recordings.connect() as db:
                    rec_list = recordings.queries.get_ok_recordings_by_date(
                        db,
                        date=rec_date,
                        limit=-1,
                        offset=0,
                    )
                with open(f'stage/listen/{rec_date.isoformat()}/index.html', 'w') as f:
                    f.write(template.render(
                        date=rec_date,
                        rec_list=rec_list,
                        prev_date=prev_date,
                        next_date=next_date,
                        )
                    )
                dates_built.append(rec_date)
    return f'Successfully built {len(dates_built)} date indexes.'


@cli.command('build-months')
@click.option('-a', '--all', 'mode', flag_value='all', default=True,
              help='generate all month pages')
@log_build
def build_months(mode):
    months_built = []
    template = jinja_env.get_template('listen_month.html')

    with recordings.connect() as db:
        dates_with_counts = recordings.queries.count_ok_recordings_by_date(db)

    dates_by_start_of_month = defaultdict(Counter)

    for row in dates_with_counts:
        d = pendulum.parse(row['date'])
        dates_by_start_of_month[d.start_of('month')][d.day] = row['count']

    for month_start, day_counts in dates_by_start_of_month.items():
        prev_month_start = month_start.subtract(months=1)
        if prev_month_start not in dates_by_start_of_month.keys():
            prev_month_start = None
        next_month_start = month_start.add(months=1)
        if next_month_start not in dates_by_start_of_month.keys():
            next_month_start = None
        path = f'listen/{month_start.strftime("%Y-%m")}'
        os.makedirs(f'stage/{path}', exist_ok=True)
        with open(f'stage/{path}/index.html', 'w') as f:
            f.write(template.render(
                path=path,
                month_start=month_start,
                prev_month_start=prev_month_start,
                next_month_start=next_month_start,
                day_counts=day_counts,
                )
            )

        months_built.append(month_start.strftime('%B %Y'))

    # Build index for month pages:
    template = jinja_env.get_template('listen_all.html')

    os.makedirs(f'stage/listen/all/', exist_ok=True)
    with open(f'stage/listen/all/index.html', 'w') as f:
        f.write(template.render(
            month_counts=[
                {'month_start': month_start, 
                 'count': sum(day_counts.values())}
                for month_start, day_counts 
                in dates_by_start_of_month.items()
                ]
            )
        )

    return f'Successfully built months: {", ".join(months_built)}'
        

if __name__ == '__main__':
    cli()
