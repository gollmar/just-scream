local lookback_sec = redis.call('GET', 'config:lookback_sec')
local max_calls = tonumber(redis.call('GET', 'config:max_calls'))
local now = redis.call('TIME')[1]
local call_sid = ARGV[1]
local caller = ARGV[2]
if redis.call('SISMEMBER', 'denylist', caller) ~=0 then
    redis.call('ZADD', 'calls:denylist', now, call_sid)
    redis.call('ZADD', 'calls:reject', now, call_sid)
    return redis.call('GET', 'twiml:deny')
end
if redis.call('SISMEMBER', 'allowlist', caller) ~= 0 then
    redis.call('ZADD', 'calls:allowlist', now, call_sid)
    redis.call('ZADD', 'calls:ok', now, call_sid)
    return redis.call('GET', 'twiml:allowlist')
end
if redis.call('ZCOUNT', 'calls:ok', now - lookback_sec, '+inf') >= max_calls then
    redis.call('ZADD', 'calls:reject', now, call_sid)
    return redis.call('GET', 'twiml:reject')
end
redis.call('ZADD', 'calls:ok', now, call_sid)
return redis.call('GET', 'twiml:ok')
