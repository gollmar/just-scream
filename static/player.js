/** JS representation of the entire playlist */
let playlist = [];
let currentTrack = 0;
let autoPlayNext = false;
let hideSpam = true;


/**
 * Load a single track from the playlist into Wavesurver and marks it as the 
 * active track. Does not immediately play that track. This is called when the 
 * playlist is initially loaded to prep the initial track.
 */
const loadTrack = function (trackNum) {
    currentTrack = trackNum;
    console.debug('Loading track ' + trackNum);
    for (const track of document.querySelectorAll('.track.active')) {
        track.classList.remove('active')
    };
    let track = playlist[trackNum];
    track.el.classList.add('active');
    wavesurfer.load('/screams/' + track.filename);
}

/**
 * Calls loadTrack to load a track from the playlist into Wavesurfer, 
 * then plays it.
 */
const playTrack = function (trackNum, playnext=false, delayms=1000) {
    console.debug('Playing track ' + trackNum);
    loadTrack(trackNum);
    wavesurfer.on('ready', function () {
        wavesurfer.play();
    });
}

/** 
 * Load a playlist by traversing .track elements from the DOM and loading 
 * data attributes. 
 */
const loadPlaylist = function () { 
    let trackNum = 0;
    singleScream = document.querySelector('.scream_details');
    if (singleScream) {
        playlist.push({
            el: singleScream,
            trackNum: 0,
            filename: singleScream.dataset.filename,
        });
    };
    for (const track of document.querySelectorAll('.track')) {
        console.debug(track);
        //if (! track.classList.contains('spam') & hideSpam) {
        if (true) {
            playlist.push({
                el: track,
                trackNum: trackNum,
                filename: track.dataset.filename,
            });
            track.querySelector('.playtrack').href = 'javascript:autoPlayNext=false;playTrack(' + trackNum + ');';
            track.querySelector('.trackname').href = 'javascript:autoPlayNext=false;loadTrack(' + trackNum + ');';
            trackNum++;
        }
    }
    console.debug(playlist);
}



var wavesurfer = WaveSurfer.create({
    container: '#waveform',
    backgroundColor: 'black',
    waveColor: 'darkorange',
    progressColor: 'rebeccapurple',
    cursorWidth: 2,
    height: 128,
    barWidth: 4,
    barMinHeight: 1,
});

var playpause = document.getElementById('playpause');
playpause.addEventListener('click', function(e) {
    e.preventDefault();
    wavesurfer.playPause();
});

var playall = document.getElementById('playall');
if (playall) {
        playall.addEventListener('click', function (e) {
            e.preventDefault();
            autoPlayNext = true;
            playTrack(0);
        });
};

wavesurfer.on('finish', function () {
    if (autoPlayNext && currentTrack < playlist.length - 1) {
        playTrack(currentTrack + 1);
    }
});


/** New! pills */
let pill = document.createElement('span');


/** Load intial track */
loadPlaylist();
loadTrack(currentTrack);
