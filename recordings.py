import os
from datetime import datetime 
import sqlite3

from twilio.rest import Client
import requests
import aiosql


TWILIO_ACCOUNT_SID = os.environ['TWILIO_ACCOUNT_SID']
TWILIO_AUTH_TOKEN = os.environ['TWILIO_AUTH_TOKEN']
SCREAM_MEDIA_DIR = os.environ.get('SCREAM_MEDIA_DIR', '.')

twilio_client = Client(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN)


queries = aiosql.from_path('recordings.sql', 'sqlite3')

def connect():
    db = sqlite3.connect('screamdb.sql')
    db.row_factory = sqlite3.Row
    return db


def create_db(drop=False):
    with connect() as db:
        if drop:
            queries.drop_tables(db)
        queries.create_table_recordings(db)
        queries.create_table_recordings_temp(db)
        queries.create_table_transcriptions(db)
        queries.create_table_spam(db)
        queries.create_table_update_metadata(db)
    return


# TODO: Set date_created_after to something sensible
# TODO: What should the limit be?
# TODO: Why is there a limit?
def fetch_recording_list(start_dt=None, end_dt=None):
    with connect() as db:
        if start_dt is None:
            latest_update = queries.get_latest_update_datetime(db)
            assert len(latest_update) == 1
            start_dt = latest_update[0]['update_datetime']
            print(f'No start date included; fetching recordings from {start_dt}.')
    rec_list = twilio_client.recordings.list(
                     date_created_after=start_dt,
                     date_created_before=end_dt,
               )
    return rec_list


def stream_recording_list(start_dt=None, end_dt=None):
    with connect() as db:
        if start_dt is None:
            latest_update = queries.get_latest_update_datetime(db)
            assert len(latest_update) == 1
            start_dt = latest_update[0]['update_datetime']
            print(f'No start date included; fetching recordings from {start_dt}.')
    recordings = twilio_client.recordings.stream(
                     date_created_after=start_dt,
                     date_created_before=end_dt,
                 )
    return recordings


# TODO:
def fetch_recording_media(recording):
    uri_base = recording.uri.split('.')[0]
    media_uri_mp3 = f'https://api.twilio.com{uri_base}.mp3'
    media_uri_wav = f'https://api.twilio.com{uri_base}.wav'

    r = requests.get(media_uri_mp3)
    filename_mp3 = f'{SCREAM_MEDIA_DIR}/screams/{recording.sid}.mp3'
    filename_wav = f'{SCREAM_MEDIA_DIR}/screams/{recording.sid}.wav'
    with open(filename_mp3, 'wb') as f:
        print(f'Saving {filename_mp3}...')
        f.write(r.content)
    r = requests.get(media_uri_wav)
    with open(filename_wav, 'wb') as f:
        print(f'Saving {filename_wav}...')
        f.write(r.content)
    return (f'{recording.sid}.mp3', f'{recording.sid}.wav')


def fetch_recording(update_metadata_id, recording, moderation_queue='default'):
    '''Downloads recording media and saves recording to database, marking it
    as queued in the specified moderation queue.'''
    with connect() as db:
        recording_id = queries.get_recording_id_by_sid(
            db,
            sid=recording.sid,
        )
        if recording_id is not None:
            print(f'Recording {recording_id} already fetched.')
            return recording_id

        try:
            filename_mp3, filename_wav = fetch_recording_media(recording)
        except:
            raise
        try:
            recording_id = queries.save_recording_metadata(
                db,
                sid=recording.sid,
                uri=recording.uri,
                date_created=recording.date_created,
                duration=int(recording.duration),
                filename_mp3=filename_mp3,
                filename_wav=filename_wav,
                update_metadata_id=update_metadata_id
            )
        except:
            raise
        queries.add_to_moderation_queue(
            db, 
            recording_id=recording_id,
            queue=moderation_queue,
        )
    return recording_id


def process_recording_list(recordings):
    with connect() as db:
        update_metadata_id = queries.save_update_metadata(db)
        i = 0
        for recording in recordings:
            i += 1
            try:
                filename_mp3, filename_wav = fetch_recording_media(recording)
            except:
                raise
            print(f'Saving {recording.sid} to temp table...')
            queries.save_recording_temp(
                db,
                sid=recording.sid,
                uri=recording.uri,
                date_created=recording.date_created,
                duration=int(recording.duration),
                filename_mp3=filename_mp3,
                filename_wav=filename_wav
            )
        print(f'Successfully downloaded media for {i} recordings.')

        try:
            queries.move_recordings_from_temp_table(
                db,
                update_metadata_id=update_metadata_id,
            )
            queries.truncate_recordings_temp(db)
        except:
            raise
    return


def get_recordings(start_dt=datetime(2020,9,1), end_dt=None, 
                   limit=50, page=1, 
                   spam=False,
                   slug=None,
                   new=False,
                   ):
    '''Pages are 1-indexed'''
    if end_dt is None:
        end_dt = datetime.now()
    query = queries.get_all_recordings
    if new:
        query = queries.get_recordings_in_datetime_range
    if spam:
        query = queries.get_spam_in_datetime_range
    if slug is not None:
        query = queries.get_playlist_recordings_by_slug
        limit = -1
    with connect() as db:
        rec_list = query(
            db, 
            start_dt=start_dt, 
            end_dt=end_dt,
            limit=limit,
            slug=slug,
            offset=limit*(page-1),
        )
    print(f'Page {page}: {len(rec_list)} recordings')
    return rec_list


def get_recording_by_id(recording_id):
    with connect() as db:
        return queries.get_recording_by_id(db, recording_id=recording_id)

def get_playlist_by_slug(slug):
    with connect() as db:
        return queries.get_playlist_by_slug(db, slug=slug)

def get_playlists(is_published=1):
    with connect() as db:
        return queries.get_playlists(
            db,
            is_published=is_published,
        )

def get_recording_playlists(recording_id):
    with connect() as db:
        return queries.get_recording_playlists(db, recording_id)

def get_track_count():
    with connect() as db:
        return queries.get_track_count(db)

def get_new_update_metadata_id():
    '''Inserts a new row in update_metadata with the current timestamp and
    returns its id.'''
    with connect() as db:
        update_metadata_id = queries.save_update_metadata(db)
    return update_metadata_id

def get_latest_update_dt():
    '''Returns the latest update timestamp from update_metadata as a 
    datetime object.'''
    with connect() as db:
        latest_update_row = queries.get_latest_update_datetime(db)
    latest_update_str = latest_update_row[0]['update_datetime']
    latest_update_dt = datetime.fromisoformat(latest_update_str)
    return latest_update_dt


def fetch_latest_recordings():
    rec_list = fetch_recording_list()
    error_list = process_recording_list(rec_list)



def update_recording_playlists(recording_id, playlist_slugs):
    with connect() as db:
        current_playlists = queries.get_recording_playlists(db, recording_id=recording_id)
        current_playlist_slugs = [p['slug'] for p in current_playlists]
        remove = [s for s in current_playlist_slugs if s not in playlist_slugs]
        add = [s for s in playlist_slugs if s not in current_playlist_slugs]
        for slug in remove:
            queries.remove_recording_from_playlist(
                db,
                recording_id=recording_id,
                playlist_slug=slug,
            )
        for slug in add:
            queries.add_recording_to_playlist(
                db,
                recording_id=recording_id,
                playlist_slug=slug,
            )
        return queries.get_recording_playlists(db, recording_id=recording_id)

def download_recording_json(sid, uri):
    with open(f'recordings/{sid}.json', 'w') as f:
        r = requests.get(f'https://api.twilio.com{uri}')
        f.write(r.text)


    
